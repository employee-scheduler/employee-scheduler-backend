import express from 'express';
import database from '../database/connection.js';

const router = express.Router();

// Get all active staff members from a company including their phone numbers and schedule preferences:
router.get('/staff', async (req, res, next) => {
    try {
        const [staff] = await database.promise().query("SELECT * FROM Staff WHERE deleted=0");
        const staffIDs = staff.map(x => x.staff_ID);
        const [phoneNums] = await database.promise().query(`SELECT * FROM Staff_Phone WHERE staff_ID IN (${staffIDs.join()})`);
        const [schedPrefs] = await database.promise().query(`SELECT * FROM Schedule_Preference WHERE staff_ID IN (${staffIDs.join()})`);
        const fullInfo = staff.map(staff => {
            return {
                ...staff,
                phone_numbers: phoneNums.filter(pn => pn.staff_ID === staff.staff_ID),
                schedule_preferences: schedPrefs.filter(sp => sp.staff_ID === staff.staff_ID)
            }
        });
        return res.json(fullInfo);
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

// Get a staff member by their ID including their phone numbers and schedule preferences:

// Add a new staff member:

// Soft delete a staff member:

// Update a staff member's information:

export default router;