import express from 'express';
import database from '../database/connection.js';

const router = express.Router();

// Get a company's work schedule for a given week:

// Get a staff member's schedule for a given week:

/* Check a company's work schedule for a given week for any potential conflicts with staff availability and
schedule exceptions: 
(substeps:
    -select staff schedules that overlap with times where a staff member is sometimes/never available
    but excluding schedule exceptions where they are available [return scheduled time vs availability time, note, staff name]
    -select staff schedules that overlap with times where a staff member has a schedule exception where they 
    are not available [return scheduled time vs exception time, note, staff name]
)*/

export default router;