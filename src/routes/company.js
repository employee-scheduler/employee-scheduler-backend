import express from 'express';
import database from '../database/connection.js';

const router = express.Router();

// Get a company by its ID:
router.get('/company/:id', async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            'SELECT * FROM Company WHERE company_ID=?',
            [id],
            function (error, results, fields){
                if (error) throw error;
                const company = results[0];
                if (!company) {
                    return res.status(404).json(`Error: company with ID ${id} not found.`);
                };
                return res.json(item);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

export default router;