// For local dev environment:
import dotenv from 'dotenv';
dotenv.config();

import mysql from 'mysql2';

const localConfiguration = {
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PW,
    database: process.env.DATABASE_NAME,
    connectionLimit: 10,
    maxIdle: 10
};

const dbPool = mysql.createPool(localConfiguration);

export default dbPool;